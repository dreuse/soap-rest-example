# soap-rest-example

Simple project showing how to create a SOAP client and a REST endpoint using spring boot.

## To run:
* ``` mvn clean ```
* ``` mvn compile ```
* ``` mvn spring-boot:run ```

Obs: Only compatible with JDK8
