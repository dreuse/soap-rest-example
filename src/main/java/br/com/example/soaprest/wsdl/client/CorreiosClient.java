package br.com.example.soaprest.wsdl.client;

import javax.xml.bind.JAXBElement;

import correios.wsdl.ConsultaCEP;
import correios.wsdl.ConsultaCEPResponse;
import correios.wsdl.ObjectFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

@SuppressWarnings("unchecked")
public class CorreiosClient extends WebServiceGatewaySupport {

    public ConsultaCEPResponse consultaCEP(String cep) {
        ObjectFactory objectFactory = new ObjectFactory();
        ConsultaCEP consultaCEP = objectFactory.createConsultaCEP();
        consultaCEP.setCep(cep);
        JAXBElement<ConsultaCEP> request = objectFactory.createConsultaCEP(consultaCEP);

        JAXBElement<ConsultaCEPResponse> responseJaxb = (JAXBElement<ConsultaCEPResponse>) getWebServiceTemplate()
                .marshalSendAndReceive(getDefaultUri(), request,
                        new SoapActionCallback(""));

        return responseJaxb.getValue();
    }

}
