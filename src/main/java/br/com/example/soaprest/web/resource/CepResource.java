package br.com.example.soaprest.web.resource;

import javax.validation.ValidationException;

import br.com.example.soaprest.web.api.CepApi;
import br.com.example.soaprest.web.model.ApiError;
import br.com.example.soaprest.wsdl.client.CorreiosClient;
import correios.wsdl.EnderecoERP;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Log
@RestController
@RequestMapping("/api")
public class CepResource implements CepApi {

    private final CorreiosClient correiosClient;

    @Autowired
    public CepResource(CorreiosClient correiosClient) {
        this.correiosClient = correiosClient;
    }

    @Override
    public ResponseEntity<EnderecoERP> consultaCep(@PathVariable String cep) {
        if(cep.length() != 8){
            throw new ValidationException("O cep deve possuir oito dígitos.");
        }
        return ResponseEntity.ok(correiosClient.consultaCEP(cep).getReturn());
    }

    @ExceptionHandler({ValidationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ApiError handleValidationException(ValidationException ex) {
        log.warning(ex.getMessage());
        return new ApiError(ex.getMessage());
    }
}
