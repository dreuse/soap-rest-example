package br.com.example.soaprest.web.api;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import correios.wsdl.EnderecoERP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@SuppressWarnings({"squidS3655","squid:S1214","squid:S3655"})
public interface CepApi {

    Logger log = LoggerFactory.getLogger(CepApi.class);

    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @GetMapping(value = "/consulta-cep/{cep}",
            produces = {"application/json"})
    default ResponseEntity<EnderecoERP> consultaCep(@PathVariable("cep") String cep) {
        if (getObjectMapper().isPresent() && getAcceptHeader().isPresent() && getAcceptHeader().get().contains("application/json")) {
            try {
                if (getObjectMapper().isPresent()) {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("{ }", EnderecoERP.class), HttpStatus.NOT_IMPLEMENTED);
                }
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}

