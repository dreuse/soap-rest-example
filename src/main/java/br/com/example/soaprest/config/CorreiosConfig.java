package br.com.example.soaprest.config;

import br.com.example.soaprest.wsdl.client.CorreiosClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class CorreiosConfig {

    private final ApplicationProperties applicationProperties;

    @Autowired
    public CorreiosConfig(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("correios.wsdl");
        return marshaller;
    }

    @Bean
    public CorreiosClient correiosClient(Jaxb2Marshaller marshaller) {
        CorreiosClient client = new CorreiosClient();
        client.setDefaultUri(applicationProperties.getEndpointCorreios());
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}
