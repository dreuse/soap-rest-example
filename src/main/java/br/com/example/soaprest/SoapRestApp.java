package br.com.example.soaprest;

import br.com.example.soaprest.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({ApplicationProperties.class})
public class SoapRestApp {

    public static void main(String[] args) {
        SpringApplication.run(SoapRestApp.class, args);
    }

}
