package br.com.example.soaprest.wsdl.client;

import br.com.example.soaprest.SoapRestApp;
import correios.wsdl.ConsultaCEPResponse;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = SoapRestApp.class)
public class CorreiosClientTest {

    @Autowired
    private CorreiosClient correiosClient;

    @Test
    @SneakyThrows
    public void consultaCEP() {
        ConsultaCEPResponse consultaCEPResponse = correiosClient.consultaCEP("72017293");
        assertThat(consultaCEPResponse).isNotNull();
        assertThat(consultaCEPResponse.getReturn()).isNotNull();
        assertThat(consultaCEPResponse.getReturn().getCep()).isEqualTo("72017293");
    }
}
