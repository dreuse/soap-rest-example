package br.com.example.soaprest.web.resource;

import br.com.example.soaprest.SoapRestApp;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SoapRestApp.class)
public class CepResourceTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc restCepMock;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        restCepMock = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .build();
    }

    @Test
    @SneakyThrows
    public void consultaCep() {
        restCepMock.perform(get("/api/consulta-cep/72017293")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cep", is("72017293")))
                .andDo(print());
    }

    @Test
    @SneakyThrows
    public void consultaCepTamanhoInvalido() {
        restCepMock.perform(get("/api/consulta-cep/72017922293")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", notNullValue()))
                .andDo(print());
    }
}
